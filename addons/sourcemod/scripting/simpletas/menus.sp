
void Showmenu_Simpletas(int client)
{
	// make menu 
	Menu menu = new Menu(Menu_Simpletas, MENU_ACTIONS_ALL);
	menu.Pagination = MENU_NO_PAGINATION;
	menu.SetTitle("Simpletas");
	menu.AddItem("0", "New TAS");
	menu.AddItem("1", "Play TAS");
	menu.AddItem("2", "Edit TAS");
	
	// end
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int Menu_Simpletas(Menu menu, MenuAction action, int param1, int param2)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			char info[32];
			menu.GetItem(param2, info, sizeof info);
			int choice = StringToInt(info);
			
			if (choice == 0)
			{
				if (!IsPlayerAlive(param1))
				{
					PrintToChat(param1, "[STAS] You can't do that when you're dead!");
					Showmenu_Simpletas(param1);
					return 0;
				}
				g_iPlayerstate[param1] = PLAYERSTATE_RECORDING;
				Showmenu_SimpletasRecording(param1);
			}
			if (choice == 1)
			{
				Showmenu_SimpletasPlayback(param1);
			}
			if (choice == 2)
			{
				Showmenu_SimpletasEdit(param1);
			}
		}
		
		case MenuAction_End:
		{
			delete menu;
		}
	}
	return 0;
}

void Showmenu_SimpletasPlayback(int client)
{
	Menu menu = new Menu(Menu_SimpletasPlayback, MENU_ACTIONS_ALL);
	menu.SetTitle("Replays");
	
	char mapName[128];
	GetCurrentMap(mapName, sizeof mapName);
	GetMapDisplayName(mapName, mapName, sizeof mapName);
	
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof path, "%s/%s", REPLAY_PATH_SM, mapName);
	
	if (DirExists(path))
	{
		int fileCounter = 0;
		DirectoryListing dL = OpenDirectory(path);
		char fileName[64];
		while (dL.GetNext(fileName, sizeof fileName))
		{
			// TODO: don't duplicate code with Showmenu_SimpletasEdit
			if(StrEqual(fileName, ".") || StrEqual(fileName, ".."))
			{
				continue;
			}
			
			// check if file extension matches REPLAY_SUFFIX
			int dotIndex = 0;
			for (int i = sizeof(fileName) - 1; i >= 0; i--)
			{
				if (fileName[i] == '.')
				{
					dotIndex = i;
				}
			}
			
			if (!StrEqual(REPLAY_SUFFIX, fileName[dotIndex], false))
			{
				continue;
			}
			
			char choice[16];
			FormatEx(choice, sizeof choice, "%i", fileCounter);
			
			// strip file extension from fileName
			fileName[dotIndex] = '\0';
			menu.AddItem(choice, fileName);
			
			if (fileCounter >= REPLAY_MAX_FILES)
			{
				break;
			}
			fileCounter++;
		}
	}
	
	// end
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int Menu_SimpletasPlayback(Menu menu, MenuAction action, int param1, int param2)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			char info[32];
			char displayName[64];
			int style;
			menu.GetItem(param2, info, sizeof info, style, displayName, sizeof displayName);
			
			int choice = StringToInt(info);
			if (choice >= 0)
			{
				if (!LoadTAS(param1, displayName, g_alReplayData))
				{
					return 0;
				}
				
				ReadyReplayBot();
				ChangeClientTeam(param1, CS_TEAM_SPECTATOR);
				CreateTimer(0.25, Timer_SpectateBot, GetClientUserId(param1), TIMER_FLAG_NO_MAPCHANGE);
				CreateTimer(2.0, Timer_StartPlayback, INVALID_HANDLE, TIMER_FLAG_NO_MAPCHANGE);
			}
		}
		
		case MenuAction_End:
		{
			delete menu;
		}
	}
	return 0;
}

void Showmenu_SimpletasEdit(int client)
{
	Menu menu = new Menu(Menu_SimpletasEdit, MENU_ACTIONS_ALL);
	menu.SetTitle("Replays");
	
	char mapName[128];
	GetCurrentMap(mapName, sizeof mapName);
	GetMapDisplayName(mapName, mapName, sizeof mapName);
	
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof(path), "%s/%s", REPLAY_PATH_SM, mapName);
	
	if (DirExists(path))
	{
		int fileCounter = 0;
		DirectoryListing dL = OpenDirectory(path);
		char fileName[64];
		while (dL.GetNext(fileName, sizeof fileName))
		{
			if(StrEqual(fileName, ".") || StrEqual(fileName, ".."))
			{
				continue;
			}
			
			int dotIndex = 0;
			for (int i = sizeof(fileName) - 1; i >= 0; i--)
			{
				if (fileName[i] == '.')
				{
					dotIndex = i;
				}
			}
			
			if (!StrEqual(REPLAY_SUFFIX, fileName[dotIndex], false))
			{
				continue;
			}
			
			char choice[16];
			FormatEx(choice, sizeof choice, "%i", fileCounter);
			
			// strip file extension from fileName
			fileName[dotIndex] = '\0';
			menu.AddItem(choice, fileName);
			
			if (fileCounter >= REPLAY_MAX_FILES)
			{
				break;
			}
			fileCounter++;
		}
	}
	
	// end
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int Menu_SimpletasEdit(Menu menu, MenuAction action, int param1, int param2)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			char info[32];
			char displayName[64];
			int style;
			menu.GetItem(param2, info, sizeof info, style, displayName, sizeof displayName);
			
			int choice = StringToInt(info);
			if (choice >= 0)
			{
				if (!LoadTAS(param1, displayName, g_alRecording[param1]))
				{
					return 0;
				}
				
				g_iCurrentTick[param1] = g_alRecording[param1].Length - 1;
				g_iPlayerstate[param1] = PLAYERSTATE_PAUSED;
				Showmenu_SimpletasRecording(param1);
			}
		}
		
		case MenuAction_End:
		{
			delete menu;
		}
	}
	return 0;
}

void Showmenu_SimpletasRecording(int client)
{
	// make menu 
	Menu menu = new Menu(Menu_SimpletasRecording, MENU_ACTIONS_ALL);
	menu.Pagination = MENU_NO_PAGINATION;
	menu.SetTitle("Simpletas");
	
	if (g_iPlayerstate[client] != PLAYERSTATE_PAUSED)
	{
		menu.AddItem("0", "Pause");
	}
	else
	{
		menu.AddItem("0", "Resume");
	}
	
	menu.AddItem("1", "Go to previous pause");
	menu.AddItem("2", "Save TAS");
	
	char playbackSpeed[32];
	FormatEx(playbackSpeed, sizeof playbackSpeed, "%f", g_fPlaybackSpeed[client]);
	GCRemoveTrailing0s(playbackSpeed);
	Format(playbackSpeed, sizeof playbackSpeed, "Playback speed %sx", playbackSpeed);
	menu.AddItem("3", playbackSpeed);
	
	// end
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int Menu_SimpletasRecording(Menu menu, MenuAction action, int param1, int param2)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			char info[32];
			menu.GetItem(param2, info, sizeof info);
			int choice = StringToInt(info);
			
			if (choice == 0)
			{
				HandlePausing(param1);
				Showmenu_SimpletasRecording(param1);
			}
			if (choice == 1)
			{
				HandlePreviousPause(param1);
				Showmenu_SimpletasRecording(param1);
			}
			if (choice == 2)
			{
				char path[PLATFORM_MAX_PATH];
				if (SaveTAS(param1, path))
				{
					CPrintToChat(param1, "%s TAS has been saved to: {olive}\"%s\"{default}.", CHAT_PREFIX, path);
				}
				Showmenu_SimpletasRecording(param1);
			}
			if (choice == 3)
			{
				IncrementPlaybackSpeed(param1);
				Showmenu_SimpletasRecording(param1);
			}
		}
		
		case MenuAction_Cancel:
		{
			switch (param2)
			{
				case MenuCancel_Exit:
				{
					// for now, this is basically the only way to cancel the recording.
					
					// reset the movetype of the player
					if (g_iPlayerstate[param1] > PLAYERSTATE_RECORDING)
					{
						SetEntityMoveType(param1, MOVETYPE_WALK);
					}
					
					ResetRecording(param1);
					g_iPlayerstate[param1] = PLAYERSTATE_NOT_RECORDING;
				}
			}
		}
		
		case MenuAction_End:
		{
			delete menu;
		}
	}
	return 0;
}